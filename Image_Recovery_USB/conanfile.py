from conans import ConanFile, tools
import os


class Conan(ConanFile):
    name = "Image_Recovery_USB"
    description = "Image Recovery USB artifact"
    url = "None"
    license = "All rights reserved - Maschinenfabrik Reinhausen"
    topics = None

    def set_version(self):
        print(os.listdir())
        f = open("currentversion.txt", "r")
        self.version = f.read()
        #self.version = "0.000.000"

    def package(self):
        self.copy("*", dst="Image_Recovery_USB", src="images/base")
        self.copy("*", dst="Image_Recovery_USB", src="images/Recovery_USB")


    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)