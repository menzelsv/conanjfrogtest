from conans import ConanFile, tools
import os


class Conan(ConanFile):
    name = "image_sap"
    description = "Image SAP artifact"
    url = "None"
    license = "All rights reserved - Maschinenfabrik Reinhausen"
    topics = None

    def set_version(self):
        print(os.listdir())
        f = open("currentversion.txt", "r")
        self.version = f.read()
        #self.version = "0.000.000"

    def package(self):
        self.copy("*", dst="IsmConfig", src="image_sap/IsmConfig")
        self.copy("*", dst="root", src="image_sap/root")
        self.copy("*", dst="snippets", src="image_sap/snippets")
        self.copy("index.xsd", dst="", src="image_sap")
        self.copy("index.xml", dst="", src="image_sap")

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)