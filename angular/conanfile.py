from conans import ConanFile, tools
import os


class AngularConan(ConanFile):
    name = "Angular"
    options = {"platform": ["ism", "cap"]}
    description = "Angular artifact"
    url = "None"
    license = "All rights reserved - Maschinenfabrik Reinhausen"
    topics = None

    def set_version(self):
        print(os.listdir())
        f = open("currentversion.txt", "r")
        self.version = f.read()

    def package(self):
        # for option CAP
        if self.options.platform == "cap":
            self.copy("*", dst="dist-cap", src="artifacts_temp/ism/dist-cap")
        # for option ISM
        if self.options.platform == "ism":
            self.copy("*", dst="dist", src="artifacts_temp/ism/dist")

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)