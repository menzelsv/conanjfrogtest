from conans import ConanFile, tools
import os


class Conan(ConanFile):
    name = "base_test"
    description = "Base Test artifact"
    url = "None"
    license = "All rights reserved - Maschinenfabrik Reinhausen"
    topics = None

    def set_version(self):
        print(os.listdir())
        f = open("currentversion.txt", "r")
        self.version = f.read()
        #self.version = "0.000.000"

    def package(self):
        self.copy("*", dst="base_test/base", src="images/base")
        self.copy("*", dst="base_test/base/root/config/webmi/pki", src="image_sap/intermediate/root/config/webmi/pki")


    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)