from conans import ConanFile, tools
import os


class Conan(ConanFile):
    name = "Languages"
    description = "Languages artifact"
    url = "None"
    license = "All rights reserved - Maschinenfabrik Reinhausen"
    topics = None

    def set_version(self):
        print(os.listdir())
        f = open("currentversion.txt", "r")
        self.version = f.read()

    def package(self):
        self.copy("*", src="artifacts_temp/ism/languages")

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)