from conans import ConanFile, tools
import os


class Conan(ConanFile):
    name = "atvise"
    description = "Atvise artifact"
    url = "None"
    license = "All rights reserved - Maschinenfabrik Reinhausen"
    topics = None

    def set_version(self):
        print(os.listdir())
        f = open("currentversion.txt", "r")
        self.version = f.read()
        #self.version = "0.000.000"

    def package(self):
        self.copy("*", dst="GUIObfuscator", src="artifacts_temp/build_output/GUIObfuscator_output")
        self.copy("*", dst="Assets", src="artifacts_temp/Bibliothek/PROJECT/Assets")

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)