from conans import ConanFile, tools
import os


class Conan(ConanFile):
    name = "image_sap_key"
    description = "Image SAP Key artifact"
    url = "None"
    license = "All rights reserved - Maschinenfabrik Reinhausen"
    topics = None

    def set_version(self):
        print(os.listdir())
        f = open("currentversion.txt", "r")
        self.version = f.read()
        #self.version = "0.000.000"

    def package(self):
        self.copy("*", dst="root", src="image_sap_key/root")
        self.copy("index.xml", dst="", src="image_sap_key")


    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)